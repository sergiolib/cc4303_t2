#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <string.h>
#include "jsocket6.4.h"
#include "bufbox.h"
#include "Data-rtt.h"

#define MAX_QUEUE 100 /* buffers en boxes */

/* Version con threads rdr y sender
 * Implementa Stop and Wait sin seqn, falla frente a algunos errores
 * Modificado para incluir un número de secuencia
 */


int Data_debug = 0; /* para debugging */

/* Variables globales del cliente */
static int Dsock = -1;
static pthread_mutex_t Dlock;
static pthread_cond_t  Dcond;
static pthread_t Drcvr_pid, Dsender_pid;
static unsigned char ack[DHDR] = {0, ACK, 0, 0};

static void *Dsender(void *ppp);
static void *Drcvr(void *ppp);


#define max(a, b) (((a) > (b))?(a):(b))

/* Variables definidos por los alumnos */
#define SWS 50
#define ALFA 0.125
#define BETA 0.25
#define ABSMAX 3.0
#define ABSMIN 0.05
#define NSEQ 256
double rtt_sample[NSEQ];
double rtt;
double enviado[NSEQ];
double diff[NSEQ];
double mdev;
double delta_timeout;
unsigned char first_ack_recieved;
unsigned char LAR, LFS;
unsigned char last; /*LFS mapeado en el arreglo pending_buf */
unsigned char first; /*LAR +1 mapeado en el arreglo pending_buf*/
struct {
  BUFBOX *rbox, *wbox; /* Cajas de comunicación con el thread "usuario" */
  unsigned char pending_buf[SWS][BUF_SIZE]; /* buffer esperando ack */
  int pending_sz[SWS];  	       /* tamaño buffer esperando */
  unsigned char expecting_ack[NSEQ];    /* ack[i]==1 si estoy esperando el ack[i] */
  unsigned char expected_seq;	       /* para los paquetes enviados desde el server */
  int retries;                         /* cuantas veces he retransmitido */
  double timeout[SWS];                 /* tiempo restante antes de retransmision */
  int state;                           /* FREE, CONNECTED, CLOSED */
  int id;                              /* id conexión, la asigna el servidor */
} connection;

/* Funciones utilitarias */

/* retorna hora actual */
double Now() {
  struct timespec tt;
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
  clock_serv_t cclock;
  mach_timespec_t mts;
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  tt.tv_sec = mts.tv_sec;
  tt.tv_nsec = mts.tv_nsec;
#else
  clock_gettime(CLOCK_REALTIME, &tt);
#endif
  
  return(tt.tv_sec+1e-9*tt.tv_nsec);
}

/* Inicializa estructura conexión */
int init_connection(int id) {
  int cl;
  pthread_t pid;
  int *p;
  unsigned int i;
  connection.state = CONNECTED;
  connection.wbox = create_bufbox(MAX_QUEUE); 
  connection.rbox = create_bufbox(MAX_QUEUE);
  connection.pending_sz[1] = -1;
  connection.id = id;
  connection.expected_seq=1;
  for (i = 0; i < SWS; i++)
    connection.pending_buf[i][DID] = id;
  first = 1;
  first_ack_recieved = 0;
  return id;
}

/* borra estructura conexión */
void del_connection() {
  delete_bufbox(connection.wbox);
  delete_bufbox(connection.rbox);
  connection.state = FREE;
}

/* Función que inicializa los threads necesarios: sender y rcvr */
static void Init_Dlayer(int s) {
  
  Dsock = s;
  if(pthread_mutex_init(&Dlock, NULL) != 0) fprintf(stderr, "mutex NO\n");
  if(pthread_cond_init(&Dcond, NULL) != 0)  fprintf(stderr, "cond NO\n");
  pthread_create(&Dsender_pid, NULL, Dsender, NULL);
  pthread_create(&Drcvr_pid, NULL, Drcvr, NULL);
}

/* timer para el timeout */
void tick() {
  return;
}

/* Función que me conecta al servidor e inicializa el mundo */
int Dconnect(char *server, char *port) {
  int s, cl, i;
  struct sigaction new, old;
  unsigned char inbuf[DHDR], outbuf[DHDR];
  
  if(Dsock != -1) return -1;
  
  s = j_socket_udp_connect(server, port); /* deja "conectado" el socket UDP, puedo usar recv y send */
  if(s < 0) return s;
  
  /* inicializar conexion */
  bzero(&new, sizeof new);
  new.sa_flags = 0;
  new.sa_handler = tick;
  sigaction(SIGALRM, &new, &old);
  
  outbuf[DTYPE] = CONNECT;
  outbuf[DID] = 0;
  outbuf[DSEQ] = 0; /* El primer paquete tiene SEQ == 0 Los demas parten en SEQ==1*/
  for(i=0; i < RETRIES; i++) {
    outbuf[DRET]=i; /* asigna el numero de intentos */
    send(s, outbuf, DHDR, 0);
    alarm(INTTIMEOUT);
    if(recv(s, inbuf, DHDR, 0) != DHDR) continue;
    if(Data_debug) fprintf(stderr, "recibo: %c, %d\n", inbuf[DTYPE], inbuf[DID]);
    alarm(0);
    if(inbuf[DTYPE] != ACK || inbuf[DSEQ] != 0) continue;
    cl = inbuf[DID];
    break;
  }
  sigaction(SIGALRM, &old, NULL);
  if(i == RETRIES) {
    fprintf(stderr, "no pude conectarme\n");
    return -1;
  }
  fprintf(stderr, "conectado con id=%d\n", cl);
  init_connection(cl);
  Init_Dlayer(s); /* Inicializa y crea threads */
  return cl;
}

/* Lectura */
int Dread(int cl, char *buf, int l) {
  int cnt;
  
  if(connection.id != cl) return -1;
  
  cnt = getbox(connection.rbox, buf, l);
  return cnt;
}

/* escritura */
void Dwrite(int cl, char *buf, int l) {
  if(connection.id != cl || connection.state != CONNECTED) return;
  
  putbox(connection.wbox, buf, l);
  /* el lock parece innecesario, pero se necesita:
   * nos asegura que Dsender está esperando el lock o el wait
   * y no va a perder el signal! 
   */
  pthread_mutex_lock(&Dlock);
  pthread_cond_signal(&Dcond); 	/* Le aviso a sender que puse datos para él en wbox */
  pthread_mutex_unlock(&Dlock);
}

/* cierra conexión */
void Dclose(int cl) {
  if(connection.id != cl) return;
  
  close_bufbox(connection.wbox);
  close_bufbox(connection.rbox);
}

/*
 * Aquí está toda la inteligencia del sistema: 
 * 2 threads: receptor desde el socket y enviador al socket 
 */

/* lector del socket: todos los paquetes entrantes */
static void *Drcvr(void *ppp) { 
  int cnt;
  int cl, p;
  unsigned char inbuf[BUF_SIZE];
  int found;
  unsigned char i;
  unsigned char rets;
  unsigned char np1;
  /* Recibo paquete desde el socket */
  while((cnt=recv(Dsock, inbuf, BUF_SIZE, 0)) > 0) {
    if(Data_debug)
      fprintf(stderr, "recv: id=%d, type=%c, seq=%d\n", inbuf[DID], inbuf[DTYPE], inbuf[DSEQ]);
    if(cnt < DHDR) continue;
    
    cl = inbuf[DID];
    if(cl != connection.id) continue;
    
    pthread_mutex_lock(&Dlock);
    if(inbuf[DTYPE] == CLOSE) { // El server me esta pidiendo que cerremos la conexion. Mando un ACK 
      if(Data_debug) fprintf(stderr, "recibo cierre conexión %d, envío ACK\n", cl);
      ack[DID] = cl;
      ack[DTYPE] = ACK;
      ack[DSEQ] = inbuf[DSEQ];
      ack[DRET] = 1;
      if(send(Dsock, ack, DHDR, 0) < 0) {
	perror("send"); exit(1);
      }
      if(inbuf[DSEQ] != LAR+1) {
	pthread_mutex_unlock(&Dlock);
	continue;
      }
      connection.state = CLOSED;
      Dclose(cl);
    }
    else if(inbuf[DTYPE] == ACK && connection.state != FREE) { //El server envia un ACK para uno de los paquetes enviados
      // Doy por recibidos ok todos los paquetes con DSEQ LAR<x<=inbuf[DSEQ]
      if (LAR < inbuf[DSEQ]) { // desde LAR+1 hasta inbuf[DSEQ]. Chequear si la ventana hacer overflow al largo de los numeros de SEQ
	np1 = (LAR + 1)%NSEQ;
	for (i = np1; i < inbuf[DSEQ]; i++)
	  connection.expecting_ack[i] = 0;
	first = (first+ (inbuf[DSEQ]-LAR))%SWS; //Actualizo el indice del primer elemento de la ventana (pending_buf)
	LAR = inbuf[DSEQ]; // Actualizo LAR
	if (first_ack_recieved == 0) {
	  rtt = Now() - enviado[LAR];
	  mdev = rtt/2.0;
	  enviado[LAR] = -1;
	  first_ack_recieved = 1;
	  if(Data_debug)
	    fprintf(stderr, "RTT estimado: %f ms\n",rtt*1000.0);
	}
	if (enviado[LAR] > 0) {
	  rtt_sample[LAR] = Now() - enviado[LAR]; // Actualizo rtt_sample
	  diff[LAR] = rtt_sample[LAR] - rtt; // actualizo diferencia
	  rtt += ALFA*diff[LAR]; // actualizo rtt si y solo si el paquete no es retransmitido
	  if(Data_debug)
	    fprintf(stderr, "RTT estimado: %f ms\n",rtt*1000.0);
	  mdev = (1-BETA)*mdev + BETA*fabs(diff[LAR]); // actualizo la desviacion estandar
	}
	if(Data_debug)
	  fprintf(stderr, "recv ACK id=%d, seq=%d\n", cl, inbuf[DSEQ]);
	if(connection.state == CLOSED) {
	  /* conexion cerrada y sin buffers pendientes */
	  del_connection();
	}
      }
      else if ((LAR+SWS-NSEQ) > inbuf[DSEQ]) { //caso de desborde donde inbuf[DSEQ] esta antes que LAR
	np1 = (LAR + 1)%NSEQ;
	for (i = np1; i < inbuf[DSEQ]; i++)
	  connection.expecting_ack[i] = 0;
	first = (first + inbuf[DSEQ]+(NSEQ-LAR))%SWS; //Actualizo el indice del primero en la ventana pending_buf
	LAR = inbuf[DSEQ]; // Actualizo LAR
	if (enviado[LAR] > 0) {
	  rtt_sample[LAR] = Now() - enviado[LAR]; // Actualizo rtt_sample
	  diff[LAR] = rtt_sample[LAR] - rtt; // actualizo diferencia
	  rtt += ALFA*diff[LAR]; // actualizo rtt si y solo si el paquete no es retransmitido
	  if(Data_debug)
	    fprintf(stderr, "RTT estimado: %f ms\n",rtt*1000.0);
	  mdev = (1-BETA)*mdev + BETA*fabs(diff[LAR]); // actualizo la desviacion estandar
	}
	if(Data_debug)
	  fprintf(stderr, "recv ACK id=%d, seq=%d\n", cl, inbuf[DSEQ]);
	if(connection.state == CLOSED) {
	  /* conexion cerrada y sin buffers pendientes */
	  del_connection();
	}
      }
      //fprintf(stderr ,"LAR: %d\n", LAR);
      pthread_cond_signal(&Dcond);
    }
    else if(inbuf[DTYPE] == DATA && connection.state == CONNECTED) {
      if(Data_debug) fprintf(stderr, "rcv: DATA: %d, seq=%d, expected=%d\n", inbuf[DID], inbuf[DSEQ],connection.expected_seq );
      if(boxsz(connection.rbox) >= MAX_QUEUE) { /* No tengo espacio */
	pthread_mutex_unlock(&Dlock);
	continue;
      }
      
      ack[DID] = cl; // fabrico ack con id cl
      ack[DTYPE] = ACK; // tipo ack
      if (inbuf[DSEQ] == connection.expected_seq) { // si me llego la secuencia que esperaba
	ack[DSEQ] = inbuf[DSEQ]; // ack será para secuencia nueva
	ack[DRET] = 1; // retry 1
	rets= ack[DRET];
	connection.expected_seq = (connection.expected_seq+1)%NSEQ;//muevo mi ventan de recepcion
        if(Data_debug) fprintf(stderr, "Enviando ACK %d, seq=%d\n", ack[DID], ack[DSEQ]);
        if(send(Dsock, ack, DHDR, 0) <0)
	perror("sendack");
      
        /* enviar a la cola */
        putbox(connection.rbox, (char *)inbuf+DHDR, cnt-DHDR);
      } else { // si no era el que esperaba
	ack[DSEQ] = connection.expected_seq-1 > 0? connection.expected_seq-1 : connection.expected_seq+NSEQ-1; // mando un ACK de expected_seq, de modo de avisar que no me llego 
	rets++;
	ack[DRET] = rets; // aumento el numero de retransmisiones 
        if(Data_debug) fprintf(stderr, "Enviando ACK %d, seq=%d\n", ack[DID], ack[DSEQ]);
        if(send(Dsock, ack, DHDR, 0) <0)
	perror("sendack");
      }
    }
    else if(Data_debug) {
      fprintf(stderr, "descarto paquete entrante: t=%c, id=%d\n", inbuf[DTYPE], inbuf[DID]);
    }
    
    pthread_mutex_unlock(&Dlock);
  }
  fprintf(stderr, "fallo read en Drcvr()\n");
  return NULL;
}

double Dclient_timeout_or_pending_data() {
  int cl, p;
  double timeout;
  /* Suponemos lock ya tomado! */
  
  timeout = Now()+20.0;
  if(connection.state == FREE) return timeout;
  unsigned char next;
  next = (LAR+1)%NSEQ;
  if(boxsz(connection.wbox) != 0 && connection.expecting_ack[next] == 0)
    /* data from client */
    return Now();
  
  if (connection.expecting_ack[first] == 0)
    return timeout;
  
  if(connection.timeout[first] <= Now()) return Now();
  if(connection.timeout[first] < timeout) timeout = connection.timeout[first];
  return timeout;
}

/* Thread enviador y retransmisor */
static void *Dsender(void *ppp) { 
  double timeout;
  struct timespec tt;
  int p;
  int ret;
  unsigned int i;
  unsigned int full;
  unsigned char next;
  unsigned char LFS_prior;
  for(;;) {
    pthread_mutex_lock(&Dlock);
    /* Esperar que pase algo. Se cumplio un timeout (retransmitir) o estoy listo para enviar un paquete (correr la ventana en 1 paquete) */
    while((timeout=Dclient_timeout_or_pending_data()) > Now()) {
      // fprintf(stderr, "timeout=%f, now=%f\n", timeout, Now());
      // fprintf(stderr, "Al tuto %f segundos\n", timeout-Now());
      tt.tv_sec = timeout;
      // fprintf(stderr, "Al tuto %f nanos\n", (timeout-tt.tv_sec*1.0));
      tt.tv_nsec = (timeout-tt.tv_sec*1.0)*1000000000;
      // fprintf(stderr, "Al tuto %f segundos, %d secs, %d nanos\n", timeout-Now(), tt.tv_sec, tt.tv_nsec);
      ret=pthread_cond_timedwait(&Dcond, &Dlock, &tt);
      // fprintf(stderr, "volvi del tuto con %d\n", ret);
    }
    
    /* Revisar clientes: timeouts y datos entrantes */
    
    if(connection.state == FREE) continue;
    if(LFS >= LAR){ //si se han enviado 50 paquetes full==1
	full=(LFS-LAR)>=SWS;
    }
    else{
	full=((NSEQ-LAR)+LFS-1)>=SWS;
    }
    if(connection.expecting_ack[next] && full && connection.state !=CLOSED) { /* retransmitir si después de timeout no me ha llegado ack de LAR+1 */
      // retransmito ventana de largo SWS, desde LAR + 1 hasta LAR + SWS, LFS al final igual a LAR + SWS
      // existe una condicion de borde: Puede ocurrir que el timeout de un paquete LAR+k sea menor que el de LAR+1 (debido al proceso de estimacion. Hay que asegurarse que se cumplio el timeout del paquete que tenia un timeout mas pequeño.
      if(Data_debug) fprintf(stderr, "TIMEOUT\n");
      delta_timeout *= 2.0; // duplico timeout anterior
      if (delta_timeout > ABSMAX)
	delta_timeout = ABSMAX; // sin que supere ABSMAX
      printf("timeout calculado: %f\n", delta_timeout);
      if(++connection.retries > RETRIES) {
	fprintf(stderr, "too many retries: %d\n", connection.retries);
	del_connection();
	exit(1);
      }
      LFS_prior = LFS;
      for (i = 1; i <= SWS; i++) {
	next = (LAR + i)%NSEQ;
	enviado[connection.pending_buf[next%SWS][DSEQ]] = -1; // Tiempo de envío no lo guardo pues hubo restransmisión
//	if (LAR+i > LFS) { // si el paquete que estoy mirando no ha sido enviado antes
	if(next > LFS_prior || LFS_prior+SWS-NSEQ > next){
	  fprintf(stderr, "Cargando nueva con next=%d!\n", next);
	  //fprintf(stderr, "Rellenando ventana con seq=%d\n", next);
	  LFS = (LFS + 1)%NSEQ; // aumento en 1 el LFS para no volver a actualizar esto
	  connection.pending_sz[(last+i)%SWS] = getbox(connection.wbox, (char *)connection.pending_buf[(last+i)%SWS]+DHDR, BUF_SIZE); // como es un paquete no he mandado antes, pido el chunk que corresponde del archivo
	  connection.pending_buf[(last+i)%SWS][DRET] = 1; // retries = 1 inicialmente

	  connection.pending_buf[(last+i)%SWS][DSEQ]=next;	// número de secuencia igual al LAR+i
	  if(connection.pending_sz[(last+i)%SWS] == -1) { /* EOF */ 
	    if(Data_debug) fprintf(stderr, "sending EOF\n");
	    connection.state = CLOSED;
	    connection.pending_buf[(last+i)%SWS][DTYPE]=CLOSE;
	    connection.pending_sz[(last+i)%SWS] = 0;
	  }
	  else {
	    if(Data_debug) 
	      connection.pending_buf[(last+i)%SWS][DTYPE]=DATA;
	  }
	  
	}
	else {
	  connection.pending_buf[((first+i-1)%SWS)%SWS][DRET]++;
	}
	if(Data_debug) fprintf(stderr, "Re-send DATA %d, seq=%d\n", connection.id, connection.pending_buf[((first+i-1)%SWS)%SWS][DSEQ]);
	connection.expecting_ack[next] = 1; // marco paquete como que espero el ACK
	if(send(Dsock, connection.pending_buf[(first+i-1)%SWS], DHDR+connection.pending_sz[(first+i-1)%SWS], 0) < 0) {
	  perror("send2"); exit(1);
	}
	connection.timeout[(first+i-1)%SWS] = Now() + delta_timeout; // modifico timeout del paquete
	if (connection.state == CLOSED)
	  break;
      }
      last = (first+SWS)%SWS;
    }
    else if(boxsz(connection.wbox) != 0){// && !connection.expecting_ack[first%SWS]) {
      if (((LFS >= LAR) && LFS-LAR<SWS) || (LFS < LAR && NSEQ-LAR+LFS < SWS)) {
	/*
	  Hay un buffer para mi para enviar
	  leerlo, enviarlo, marcar esperando ACK
	*/
	LFS = (LFS+1)%NSEQ; // LFS es mi nuevo paquete a enviar
	last= (last+1)%SWS;
	connection.pending_sz[last] = getbox(connection.wbox, (char *)connection.pending_buf[last]+DHDR, BUF_SIZE); 
	connection.pending_buf[last][DRET]=1; // Retries 1
	connection.pending_buf[last][DSEQ]=LFS; // Numero de secuencia del paquete igual a LFS
	if(connection.pending_sz[last] == -1) { /* EOF */ 
	  if(Data_debug) fprintf(stderr, "sending EOF\n");
	  connection.state = CLOSED;
	  connection.pending_buf[last][DTYPE]=CLOSE;
	  connection.pending_sz[last] = 0;
	}
	else {
	  if(Data_debug) 
	    fprintf(stderr, "sending DATA id=%d, seq=%d, retries=%d\n", connection.id, connection.pending_buf[last][DSEQ], connection.pending_buf[last][DRET]);
	  connection.pending_buf[last][DTYPE]=DATA; // Si no es final de conexión, es un dato
	}
	
	delta_timeout = rtt + 4.0*mdev; // Actualizo delta timeout
	//fprintf(stderr, "delta=%f\n", delta_timeout);
	if (delta_timeout > ABSMAX)
	  delta_timeout = ABSMAX; // el delta no puede ser mayor a ABSMAX
	else if (delta_timeout < ABSMIN)
	  delta_timeout = ABSMIN; // tampoco menor a ABSMIN
	connection.timeout[last] = Now() + delta_timeout; // en definitiva, actualizo el timeout
	send(Dsock, connection.pending_buf[last], DHDR+connection.pending_sz[last], 0); // envío el paquete
	enviado[LFS] = Now(); // marco el momento de envío para estimación de RTT
	connection.expecting_ack[LFS] = 1; // marco que estoy esperando un ACK de este paquete
	
	connection.retries = 0; // como envié un paquete satisfactoriamente, reseteo el registro de retries de la conexión
      }
    }
    pthread_mutex_unlock(&Dlock);
  }
  return NULL;
}
